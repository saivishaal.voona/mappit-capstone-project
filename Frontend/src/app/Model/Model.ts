//model class for address
export class Address{
    id:number=0;
    address1:string="";
    address2:string="";
    city:string="";
    state:string="Choose state";
    zip:number=0;
    address:string=''
    addressTitle:string='';
    addressDesc:string='';
    link:string=''
}
