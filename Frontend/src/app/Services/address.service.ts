import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { Address } from '../Model/Model';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  //api key for raising request to geocoding api
  apiKey='AIzaSyD3Wtqr6HeilllG5BkVom-egKV-i5Ylke8';
  address: Address;
  constructor(private http: HttpClient) {
    this.address = new Address();
  }
  //method for sending post address request to REST api
  addData(addressDetails: any): Observable<any> {

    return this.http.post<any>('http://localhost:8085/api/addresses', addressDetails)
  }

//method for sending post address from file request to REST api
  saveDataFromFile(field: any): Observable<any> {

    return this.http.post<any>('http://localhost:8085/api/addresses', field)
  }

  //method for sending get similar search results request to REST api
  fetchFromDb(keyword:string){
    return this.http.get<Address[]>('http://localhost:8085/api/similarsearch',{params:new HttpParams().set('keyword',keyword)})
  }

  //method for sending get all addresses request to REST api
  getAllAddresses(){
    return this.http.get<Address[]>('http://localhost:8085/api/getalldata')

  }

  //method for validating address by sending get request to geocoding api with key and address as params
  isValidAddress(address:string){
    let httpParam=new HttpParams()
    httpParam=httpParam.append('address',address)
    httpParam=httpParam.append('key',this.apiKey)

    return this.http.get<any>('https://maps.googleapis.com/maps/api/geocode/json',{params:httpParam})

  }


}
