import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { AddressService } from './address.service';
import { Address } from '../Model/Model';

describe('AddressService (HttpClientTestingModule)', () => {
  let service: AddressService;
  let httpTestingController: HttpTestingController;
  let address:Address;
  //injecting address service and http tesing controller
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[AddressService],
      imports: [HttpClientTestingModule],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AddressService);
    address={
      id:34,
      address1:"",
      address2:"",
      city:"",
      state:"",
      zip:0,
      address:'9, Old China Bazar Street, Kolkata, West Bengal, 700001',
      addressTitle:'',
      addressDesc:'',
      link:''

    }
  });
  //testing fetch from DB similar search method
  describe('fetchFromdb()',()=>{
    it('should return address when fetchfromDb is called with keyword', () => {
      service.fetchFromDb("China").subscribe();
      let url ="http://localhost:8085/api/similarsearch?keyword=China"
      const request = httpTestingController.expectOne(url);
      expect(request.request.params.get("keyword")).toBe("China");
      console.log(request.request.url);
      expect(request.request.method).toBe('GET');
    });
  });

  //testing add address method
  describe('addData()',()=>{
    it('should add address when AddData is called ', () => {
      service.addData(address).subscribe();
      let url ="http://localhost:8085/api/addresses"
      const request = httpTestingController.expectOne(url);
      expect(request.request.method).toBe('POST');
      expect(request.request.body).toEqual(address)
    });
  });

  //testing add addresses from file method
  describe('saveDataFromFile()',()=>{
    it('should add multiple address when saveDataFromFile is called ', () => {
      service.saveDataFromFile(address).subscribe();
      let url ="http://localhost:8085/api/addresses"
      const request = httpTestingController.expectOne(url);
      expect(request.request.method).toBe('POST');
      expect(request.request.body).toEqual(address)
    });
  });

  afterEach(() => {
    httpTestingController.verify();
  });
});



