import { NgModule } from '@angular/core';
import { SearchComponent } from './Components/search/search.component';
import { AddAddressComponent } from './Components/add-address/add-address.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'Home',component:HomeComponent},
  { path: 'Search', component: SearchComponent },
  { path: 'Add-Address', component: AddAddressComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
