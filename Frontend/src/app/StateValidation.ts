import { AbstractControl } from "@angular/forms";
//validating the state default value
export class StateValidation {

  static MatchState(AC: AbstractControl) {
     const formGroup = AC.parent;
     if (formGroup) {
          const stateInput = formGroup.get('state');
          if (stateInput) {
              const state = stateInput.value;

              if (state == "Choose state") {
                  return { matchState: true };
              } else {
                  return null;
              }
          }
     }

     return null;
  }
}
