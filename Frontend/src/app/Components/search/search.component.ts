import { Component, OnInit } from '@angular/core';
import { AddressService } from 'src/app/Services/address.service';
import { Address } from 'src/app/Model/Model';
import { FormControl, Validators } from '@angular/forms';
import { map, Observable } from 'rxjs';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchtext!: string;
  similarSearchAddresses:Array<Address>=[];
  searchFlag:boolean=true;
  length:number=0;
  numOfSimilarAdd=0;

  mapsLink="https://www.google.com/maps/search/";

  searchTerm= new FormControl();



  options:Array<String>=[];
  filteredOptions: Observable<any[]> | undefined;
  toHighlight='';

  constructor(private service: AddressService) {
    //validation for restricting spl chars in user input
    this.searchTerm.addValidators(Validators.pattern('^[^!@#$%^&*|~]+$'))

  }

  ngOnInit(): void {
    //for fetching all the addresses from DB into options var
    this.service.getAllAddresses().subscribe(resp=> {
      resp.map(obj=>{
        this.options.push(obj.address)

      })
    })

    //filtering the options var based on the user input into filteredOptions
    this.filteredOptions=this.searchTerm.valueChanges.pipe(
      map((term)=>{
        this.toHighlight=term;
        return this.options.filter((option)=> option.toLowerCase().includes(term.toLowerCase()))
      })
    )
  }

  //search method for similar search results
  public search(){

    this.service.fetchFromDb(this.searchTerm.value)
    .subscribe(resp=>{
      this.similarSearchAddresses=resp
      this.numOfSimilarAdd=resp.length;


      for(let addr of this.similarSearchAddresses){
        let stringvar=addr.address;
        let adrresspart=stringvar.split(',')
        addr.link=this.mapsLink+addr.address
        addr.addressTitle=adrresspart[0]+", "+adrresspart[1];
        addr.addressDesc=adrresspart[adrresspart.length-3]+", "+adrresspart[adrresspart.length-2]+", "+adrresspart[adrresspart.length-1];
      }
    }
    )
    this.searchFlag=false;

  }

  }



