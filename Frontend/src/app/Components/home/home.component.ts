import { Component, HostListener, OnInit } from '@angular/core';



@Component({

  selector: 'app-home',

  templateUrl: './home.component.html',

  styleUrls: ['./home.component.css']

})

export class HomeComponent implements OnInit {

  mobile:boolean=false;

  constructor() { }



  ngOnInit(): void {

    //check for mobile mode
    if (window.screen.width <= 768) {
        this.mobile = true;
    }

  }



  @HostListener('window:resize', ['$event'])
//check for mobile mode while changing the screen size responsively
onResize($event:any) {

  if(window.screen.width <=768){

    this.mobile = true;

  }
  else{

    this.mobile = false;

  }

}
}
