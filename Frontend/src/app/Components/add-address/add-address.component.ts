import { Component, OnInit } from '@angular/core';
import { Address } from 'src/app/Model/Model';
import { AddressService } from 'src/app/Services/address.service';
import * as XLSX from 'xlsx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StateValidation } from 'src/app/StateValidation';
@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.css'],
})
export class AddAddressComponent implements OnInit {
  address: Address;
  addressForm: FormGroup;
  fileContent: string | any = '';
  file = new File([], '');
  formFlag: boolean = true;
  isAddressValid: boolean = false;
  isAddressSaved: boolean = false;
  isFileSizeValid:boolean=false;

  constructor(
    private service: AddressService,
    private formBuilder: FormBuilder
  ) {
    this.address = new Address();

    //address form validation
    this.addressForm = this.formBuilder.group({
      address1: [
        '',
        Validators.compose([Validators.required, Validators.minLength(3),Validators.maxLength(150)]),
      ],
      address2: ['',Validators.compose([Validators.maxLength(100)])],
      city: [
        '',
        Validators.compose([Validators.required, Validators.minLength(3),Validators.maxLength(20)]),
      ],
      state: [
        '',
        Validators.compose([Validators.required, Validators.minLength(3),StateValidation.MatchState]),
      ],
      zip: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('[0-9]{6}'),
        ]),
      ],
    });
  }

  ngOnInit(): void {}
  filecloser() {
    this.formFlag = !this.formFlag;
  }

  //method for adding single address into the DB
  AddDetails(addressForm: FormGroup): void {
    let addressDetail = '';
    if (this.address.address2 == '') {
      addressDetail =
        this.address.address1 +
        ', ' +
        this.address.city +
        ', ' +
        this.address.state +
        ', ' +
        this.address.zip;
    } else {
      addressDetail =
        this.address.address1 +
        ', ' +
        this.address.address2 +
        ', ' +
        this.address.city +
        ', ' +
        this.address.state +
        ', ' +
        this.address.zip;
    }

    let addr = { address: addressDetail };
    //validating the address user has given in form
    this.service.isValidAddress(addressDetail).subscribe((resp) => {
      if (resp.status == 'OK') {
        this.isAddressValid = true;
      }

      if (this.isAddressValid) {
        this.service.addData(addr).subscribe(
          (resp) => {
            alert('Address saved successfully!!');

          },
          (error) => {
            if (error.status == 400) {
              alert('Address already exists!! Please add a new address.');
            } else {
              alert('Something went wrong!! Please try again later.');
            }
          }
        );
        this.addressForm.reset();

      } else {
        alert(
          'The address you have entered is not valid. Please enter a valid address '
        );
      }
    });
  }
// assigning file once read from UI
  public readFile(event: any): void {

    this.file = event.target.files[0];

  }

//method for saving all addresses from file into DB
  uploadFile(event:any) {
    //file size validation
    if(this.file.size>2*1024*1024){

      alert("Max file size is 2MB")
    }
    //reading the file and extracting the content
    else{
      let fileReader: FileReader = new FileReader();
    fileReader.readAsBinaryString(this.file);

    fileReader.onload = (x) => {
      var workBook = XLSX.read(fileReader.result, { type: 'binary' });
      var sheetNames = workBook.SheetNames;
      this.fileContent = XLSX.utils.sheet_to_json(
        workBook.Sheets[sheetNames[0]]
      );
      let isFileInvalid = false;
      //validating all addresses from file using geocoding api
      for (var field of this.fileContent) {
        this.service.isValidAddress(field.address).subscribe((error) => {
          if (error.status != 'OK') {
            isFileInvalid = true;
          }
        });
      }
      setTimeout(()=>{if (!isFileInvalid) {
        let successCounter = 0;
        let failDupCounter = 0;
        let backendFail = false;

        //saving each and every address from file into Db

        for (var field of this.fileContent) {
          this.service.saveDataFromFile(field).subscribe(
            (resp) => {
              successCounter += 1;
            },
            (error) => {

              if (error.status == 400) {
                failDupCounter += 1;
              } else {
                backendFail = true;
              }
            }
          );
        }

        setTimeout(() => {
          if (backendFail) {
            alert('Something went wrong! Please try again later.');
          } else {
            alert(
              successCounter +
                ' addresses added succesfully, ' +
                failDupCounter +
                ' addresses already exists!!'
            );
          }
        }, 100);

      }
      else{
        alert("Some of the addresses in the file are not valid!!")
      }

      },500)

      // event.target.value = null;
      this.file = new File([], '');

    };


    }

  }
}
