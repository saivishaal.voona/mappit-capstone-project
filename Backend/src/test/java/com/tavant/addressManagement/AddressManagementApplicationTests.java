package com.tavant.addressManagement;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.tavant.addressManagement.model.Address;
import com.tavant.addressManagement.repository.AddressRepository;
import com.tavant.addressManagement.service.AddressService;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
class AddressManagementApplicationTests {
	@Autowired
    private AddressService servi;
    
    @MockBean
    private AddressRepository repo;
    
    //test for saving the address
    @Test
    @DisplayName("Save Address Test")
    public void saveaddTest() throws Exception
    {
        Address a=new Address(200L, "23/1 2nd Floor, Delhi, Delhi, 110008");
        when(repo.save(a)).thenReturn(a);
        assertEquals(a,servi.createAddress(a));
        
    }
    
    //test for getting all the addresses
    @Test
    @DisplayName("Get all addresses Test")
    public void getAllTest()
    {
        
        when(repo.findAll()).thenReturn(Stream.of(
        		new Address(300L, "9, Old China Bazar Street, Delhi, Delhi, 700001"),
        		new Address(400L, "234 A, Chandni Plaza Sant Nagar, Delhi, Delhi, 110065")).collect(Collectors.toList()));
        assertEquals(2, servi.allData().size());
        
    }
    
    
    //test for getting similar search results
    @Test
    @DisplayName("Similar 5 Test")
    public void similarfiveTest()
    {
        String s="delhi";
        when(repo.similarSearch(s)).thenReturn(Stream.of(
        		new Address(300L, "9, Old China Bazar Street, Delhi, Delhi, 700001"),
        		new Address(400L, "234 A, Chandni Plaza Sant Nagar, Delhi, Delhi, 110065")).collect(Collectors.toList()));
        assertEquals(2, servi.similarSearch(s).size());
        
    }

	

}
