package com.tavant.addressManagement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tavant.addressManagement.controller.AddressController;
import com.tavant.addressManagement.model.Address;
import com.tavant.addressManagement.service.AddressService;

@RunWith(SpringRunner.class)
@WebMvcTest(AddressController.class)
public class AddressControllerTest {

	@MockBean
	private AddressService addressService;
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;
	
	//tesing save address method from controller

	@Test
	void shouldCreateAddress() throws Exception {
		Address mockAddress = new Address(200L, "23/1 2nd Floor, Delhi, Delhi, 110008");
		String inputInJson = this.mapToJson(mockAddress);

		String URI = "/api/addresses";

		Mockito.when(addressService.createAddress(Mockito.any(Address.class))).thenReturn(mockAddress);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).accept(MediaType.APPLICATION_JSON)
				.content(inputInJson).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		String outputInJson = response.getContentAsString();

		assertThat(outputInJson).isEqualTo(inputInJson);
		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}

	
	//fetching all data testing from controller
	@Test
	public void shouldGetAllAddresses() throws Exception {

		Address address1 = new Address(300L, "9, Old China Bazar Street, Kolkata, West Bengal, 700001");
		Address address2 = new Address(400L, "234 A, Chandni Plaza Sant Nagar, Delhi, Delhi, 110065");

		List<Address> addressList = new ArrayList<>();
		addressList.add(address1);
		addressList.add(address2);

		Mockito.when(addressService.allData()).thenReturn(addressList);

		String URI = "/api/getalldata";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

//	       Address address3 = new Address(600L,"gwrtehrs");
//		     
//	       addressList.add(address3);

		String expectedJson = this.mapToJson(addressList);
		String outputInJson = result.getResponse().getContentAsString();
		assertThat(outputInJson).isEqualTo(expectedJson);
	}

	
	//fetching simlar addresses testing from controller
	@Test
	public void shouldGetSimilarAddresses() throws Exception {

		Address address1 = new Address(300L, "9/72, Chawri Bazar, Bangolore, Karnataka, 110006");
		Address address2 = new Address(400L, "234 A, Chandni Plaza Sant Nagar, Delhi, Delhi, 110065");

		List<Address> addressList = new ArrayList<>();
		addressList.add(address1);
		addressList.add(address2);
		
		List<Address> addressList1 = new ArrayList<>();
		addressList.add(address2);

		Mockito.when(addressService.similarSearch(Mockito.anyString())).thenReturn(addressList1);

		String URI = "/api/similarsearch?keyword={keyword}";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI,"Delhi").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

//		Address address3 = new Address(300L, "gwrtehrs");
//
//		addressList.add(address3);

		String expectedJson = this.mapToJson(addressList);
		String outputInJson = result.getResponse().getContentAsString();
		System.out.println(expectedJson);
		System.out.println(outputInJson);
		assertThat(outputInJson).isEqualTo(expectedJson);
	}

	/**
	 * Maps an Object into a JSON String. Uses a Jackson ObjectMapper.
	 */
	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}

}
