package com.tavant.addressManagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tavant.addressManagement.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
	
	// method for fetching the similar addresses based on keyword
	@Query(value = "SELECT * FROM addresses WHERE ts @@ to_tsquery('english', ?1) LIMIT 5;",nativeQuery = true)
	public List<Address> similarSearch(String keyword);

}
