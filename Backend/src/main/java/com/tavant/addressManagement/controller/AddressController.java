package com.tavant.addressManagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tavant.addressManagement.model.Address;
import com.tavant.addressManagement.service.AddressService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
// allowing frontend to send requests
public class AddressController {
	
	//dependency injection of service layer
	@Autowired
	AddressService addressService;
	
	
	//creating endpoint for posting a new address into DB
	@CrossOrigin
	@PostMapping(value="/addresses", consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Address> createAddress(@RequestBody Address address){
		try {
			Address addressRes=addressService.createAddress(address);
			return new ResponseEntity<Address>(addressRes,HttpStatus.OK);
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return new ResponseEntity<Address>(HttpStatus.BAD_REQUEST);
		}
		

    }
	
	//creating endpoint for fetching all records
	@GetMapping(value="/getalldata",produces = MediaType.APPLICATION_JSON_VALUE )
	public List<Address> getAllData(){
		return addressService.allData();
	}
	
	
	//creating endpoint for fetching similar search results
	@GetMapping(value="/similarsearch", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Address>> search(@Param("keyword") String keyword) {
		
		//formatting the keyword

		String formatted_key="";
		for(char curr : keyword.toCharArray()) {
			if(curr!=' ') {
				formatted_key+=curr;
				
			}
			else {
				formatted_key+='&';
			}
			
		}
		System.out.println(formatted_key);
		return new ResponseEntity<List<Address>>(addressService.similarSearch(formatted_key),HttpStatus.OK);
		//return addressService.similarSearch(formatted_key);
		
	}
    

}
