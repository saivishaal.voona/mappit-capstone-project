package com.tavant.addressManagement.model;

import javax.persistence.*;

//table in DB is constructed using this model with ID as primary key
@Entity
@Table(name = "addresses")
//address model class with ID and address as properties
public class Address {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="address_id")
        private Long id;
    
    @Column(name="address")
    private String address;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Address(Long id, String address) {
		super();
		this.id = id;
		this.address = address;
	}

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}
    

}
