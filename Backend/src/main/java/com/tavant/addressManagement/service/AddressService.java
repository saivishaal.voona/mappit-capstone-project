package com.tavant.addressManagement.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import com.tavant.addressManagement.model.Address;
import com.tavant.addressManagement.repository.AddressRepository;

@Service
public class AddressService {
	
	//dependency injection of repository
	@Autowired
	AddressRepository addressRepository;
	
	// CREATE address method of Spring JPA
    public Address createAddress(Address address) throws Exception {
    	try {
    		return addressRepository.save(address);
    		
    	}
    	catch (DataAccessException e) {
			// TODO: handle exception
    		throw new Exception("Address already exists!!!");
    		
		}
        
    }
    
    //fetch similar results method of repository
    public List<Address> similarSearch(String keyword){
    	return addressRepository.similarSearch(keyword);
    }
    
    //fetch all data method of Spring JPA
    public List<Address> allData(){
    	return addressRepository.findAll();
    }
    

}
